# M.D. CMS - Sites

Sites package for M.D. CMS

## Install

In composer.json:

```json
"require": {
    "m.d.project/mdcms-sites": "dev-master"
}
```

for local development add repository

```json
"repositories": [
    {
        "type": "path",
        "url": "local_path/m.d.project/mdcms-sites",
        "options": {
            "symlink": true
        }
    }
]
```

next update composer:

```
composer update
```

and run migration

```
php artisan migration
```

## Publish

```
php artisan vendor:publish --provider="MdProject\MdCmsSites\MdCmsSitesServiceProvider" --tag=config

php artisan vendor:publish --provider="MdProject\MdCmsSites\MdCmsSitesServiceProvider" --tag=views
```

## Site types and dynamic fields

Site types are defined in config file.
If you want to add new type from others modules add 'site_type' key in config file.<br>
Fields declared in config will automatically render when type will be selected.

Example from News module:

```php
return [
    ...
    'site_types' => [
        'news' => [
            'name' => 'News',
            'unique' => true, //only one per language
            'order' => 11, //order on select list
            'fields' => [
                [
                    'type' => 'input',
                    'settings' => [
                        'name' => 'title',
                        'label' => 'Title'
                    ]
                ],
                [
                    'type' => 'input',
                    'settings' => [
                        'name' => 'subtitle',
                        'label' => 'Subtitle'
                    ]
                ],
            ],
            'plugins' => ['seo'],
            'blocks' => true,
        ]
    ],
    ...
];
```

Name of type and label of field will automatically added to the panel translations.


All fields will be saved on 'field' property.

```php
...
$site->fields; //give array ['fieldName' => 'value', 'fieldName2' => ['val1', 'val2'], ...]
```

## Site front

### Routing

```php
# routes/web.php

MdProject\MdCmsSites\FrontRoute::load();
```

Routing for sites schould be loaded after others routes.

### Views

Any site type have own view with the same in /resources/views/sites/ (publish views before).<br>
After adding a new page type, don't forget to create the view blade file!
