<?php

Route::group([
    'prefix' => config('mdcms.panel_url').'/{lang}',
    'namespace' => '\MdProject\MdCmsSites',
    'as' => 'mdcms.panel.',
    'middleware' => ['web', 'mdcms.admin.auth']
], function () {
    Route::resource('sites', 'Controllers\SitesController')
        ->parameters(['sites' => 'id'])
        ->except(['show']);
});
