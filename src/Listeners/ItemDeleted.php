<?php

namespace MdProject\MdCmsSites\Listeners;

use MdProject\MdCmsPanel\Events\ItemDeleted as EventItemDeleted;
use MdProject\MdCmsNavigations\Repositories\NavigationRepositoryInterface;
use MdProject\MdCmsSites\SitesNavigationProvider;
use MdProject\MdCmsSites\Models\Site;

class ItemDeleted
{
    protected $navRepository;

    public function __construct(NavigationRepositoryInterface $navRepository, SitesNavigationProvider $navProvider)
    {
        $this->navRepository = $navRepository;
        $this->navProvider = $navProvider;
    }

    public function handle(EventItemDeleted $event)
    {
        $item = $event->item;
        if ($item instanceof Site) {
            $navs = $this->navRepository->search($this->navProvider->getLinkType(), ['id' => $item->id]);
            if ($navs->isNotEmpty()) {
                foreach ($navs as $nav) {
                    $this->navRepository->delete($nav->id);
                }
            }
        }
    }
}
