<?php

namespace MdProject\MdCmsSites\Repositories;

use MdProject\MdCmsSites\Repositories\SiteRepository;
use MdProject\MdCmsPanel\Traits\PanelItemEvents;

class SiteRepositoryEloquent implements SiteRepository
{
    use PanelItemEvents;

    protected $modelClass = \MdProject\MdCmsSites\Models\Site::class;

    public function __construct()
    {
        if (class_exists(\MdProject\MdCmsBlocks\Traits\Blocks::class)) {
            $this->modelClass = \MdProject\MdCmsBlocks\Traits\Blocks::class;
        }
    }

    public function getHome(string $lang = null): ?object
    {
        $query = $this->modelClass::query()->where('type', 'home');
        $this->addLang($query, $lang);

        return $query->first();
    }

    private function addLang(object $query, string $lang = null): void
    {
        if ($lang) {
            $query->where('lang', $lang);
        }
    }

    public function getSite(string $url, string $lang = null): ?object
    {
        $query = $this->modelClass::query()->where('url', $url);
        $this->addLang($query, $lang);

        return $query->first();
    }

    public function getAll($itemsOnPage = 0, string $lang = null): iterable
    {
        $query = $this->modelClass::query();
        $this->addLang($query, $lang);

        if ($itemsOnPage > 0) {
            $result = $query->paginate($itemsOnPage);
        } else {
            $result = $query->get();
        }

        return $result;
    }

    public function getDataForNavigation(string $lang = null): object
    {
        $query = $this->modelClass::query();
        $query->select('id', 'url', 'name', 'type');
        $this->addLang($query, $lang);

        return $query->get();
    }

    public function get(int $id, string $lang = null): ?object
    {
        $query = $this->modelClass::query()->where('id', $id);
        $this->addLang($query, $lang);

        return $query->first();
    }

    public function getFirstByType(string $type, string $lang = null): ?object
    {
        $query = $this->modelClass::query()->where('type', $type);
        $this->addLang($query, $lang);

        return $query->first();
    }

    public function add(array $data): object
    {
        $data['active'] ??= 0;

        $item = $this->modelClass::create($data);
        $this->fireItemAddedEvent($item);

        return $item;
    }

    public function edit(int $id, array $data): object
    {
        $data['active'] ??= 0;

        $item = $this->get($id);
        if ($item) {
            $item->update($data);
            $this->fireItemUpdatedEvent($item);
        }

        return $item;
    }

    public function delete(int $id): bool
    {
        $item = $this->get($id);
        $result = (bool)$this->modelClass::destroy($id);

        if ($result) {
            $this->fireItemDeletedEvent($item);
        }

        return $result;
    }

    public function getUsedTypes(string $lang = null, int $skipId = null): array
    {
        $query = $this->modelClass::query()->select('type')->groupBy('type');
        $this->addLang($query, $lang);
        if ($skipId) {
            $query->where('id', '!=', $skipId);
        }

        $result = $query->pluck('type')->toArray();

        return $result;
    }
}
