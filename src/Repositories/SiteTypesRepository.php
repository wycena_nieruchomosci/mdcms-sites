<?php

namespace MdProject\MdCmsSites\Repositories;
use Illuminate\Support\Facades\App;

class SiteTypesRepository
{
    public function getAllSiteTypes()
    {
        $siteTypes = [];

        foreach (config()->all() as $conf) {
            if (isset($conf['site_types']) && is_array($conf['site_types'])) {
                $siteTypes = array_merge($siteTypes, $conf['site_types']);
            }
        }

        $this->sortByOrder($siteTypes);

        return $siteTypes;
    }

    protected function sortByOrder(array &$types)
    {
        return uasort($types, function($a, $b) {
            $a['order'] ??= 99;
            $b['order'] ??= 99;

            return $a['order'] <=> $b['order'];
        });
    }

    public function getOnlySitesModuleTypes()
    {
        $types = config('mdcms_sites.site_types');
        $this->sortByOrder($types);

        return $types;
    }

    public function getDataForSelect(string $lang, int $skipId = null)
    {
        $sitesRepository = App::make('MdProject\MdCmsSites\Repositories\SiteRepository');
        $used = $sitesRepository->getUsedTypes($lang, $skipId);
        $siteTypes = $this->getAllSiteTypes();

        $selectData = [
            'options' => [],
            'disabledOptions' => []
        ];

        foreach ($siteTypes as $type => $settings) {
            $name = $settings['name'] ?? $type;
            $unique = $settings['unique'] ?? false;

            $selectData['options'][$type] = __p($name);

            if ($unique && in_array($type, $used)) {
                $selectData['disabledOptions'][] = $type;
            }
        }

        return $selectData;
    }

    public function getPluginsConfig()
    {
        $siteTypes = $this->getAllSiteTypes();

        $pluginsData = [
            'all' => [],
            'byType' => []
        ];

        foreach ($siteTypes as $type => $settings) {
            $plugins = $settings['plugins'] ?? [];

            $pluginsData['all'] = array_merge($pluginsData['all'], $plugins);
            $pluginsData['byType'][$type] = $plugins;
        }

        $pluginsData['all'] = array_unique($pluginsData['all']);

        return $pluginsData;
    }
}
