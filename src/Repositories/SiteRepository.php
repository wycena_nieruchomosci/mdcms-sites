<?php

namespace MdProject\MdCmsSites\Repositories;

interface SiteRepository
{
    public function getHome(string $lang = null): ?object;

    public function getSite(string $url, string $lang = null): ?object;

    public function getAll(int $itemsOnPage = 0, string $lang = null): iterable;

    public function getDataForNavigation(string $lang = null): object;

    public function get(int $id, string $lang = null):? object;

    public function getFirstByType(string $type, string $lang = null):? object;

    public function add(array $data): object;

    public function edit(int $id, array $data): object;

    public function delete(int $id): bool;

    public function getUsedTypes(string $lang = null, int $skipId = null): array;
}
