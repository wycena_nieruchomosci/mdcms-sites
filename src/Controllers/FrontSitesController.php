<?php

namespace MdProject\MdCmsSites\Controllers;

use MdProject\MdCmsFront\Controllers\FrontBaseController;
use MdProject\MdCmsSites\Repositories\SiteRepository;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class FrontSitesController extends FrontBaseController
{
    private $repository;
    private $navManager;
    private $lang;

    public function __construct(
        SiteRepository $repository,
        Request $request
    )
    {
        $this->repository = $repository;

        $this->lang = $request->route('lang') ?? app()->getLocale();
    }

    public function home()
    {
        /* if default language and route with lang are equal, redirect to home witout lang */
        $homeUrl = \MdProject\MdCmsSites\FrontRoute::getHomeUrl($this->lang);
        $requestUrl = request()->path() == '/' ? '/' : '/'.request()->path();
        if ($homeUrl != $requestUrl) {
            return redirect($homeUrl);
        }

        $site = $this->repository->getHome($this->lang);
        $this->setSeo($site);

        if (!$site) {
            abort('404');
        }

        $this->setNavActive($site);

        return $this->getView($site);
    }

    public function site(Request $request)
    {
        $url = $request->route('url');
        $site = $this->repository->getSite($url, $this->lang);
        $this->setSeo($site);

        if (!$site) {
            abort('404');
        } elseif ($site->type === 'home') {
            $homeUrl = \MdProject\MdCmsSites\FrontRoute::getHomeUrl($this->lang);
            return redirect($homeUrl);
        }

        $this->setNavActive($site);

        return $this->getView($site);
    }

    private function setNavActive($site)
    {
        \NavManager::setActive('site_'.$site->id);
    }

    private function getView($site)
    {
        if (View::exists('sites.'.$site->type)) {
            return view('sites.'.$site->type)->with('site', $site);
        } else {
            return view('mdcms-sites::'.$site->type)->with('site', $site);
        }
    }
}
