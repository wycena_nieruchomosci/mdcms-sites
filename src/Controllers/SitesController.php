<?php

namespace MdProject\MdCmsSites\Controllers;

use MdProject\MdCmsPanel\Controllers\Controller;
use MdProject\MdCmsSites\Repositories\SiteRepository;
use MdProject\MdCmsSites\Requests\StoreOrEditSite;
use MdProject\MdCmsSites\Repositories\SiteTypesRepository;
use MdProject\MdCmsCore\Jobs\RouteCacheReload;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class SitesController extends Controller
{
    private $repository;
    private $siteTypesRepository;

    public function __construct(
        SiteRepository $repository,
        SiteTypesRepository $siteTypesRepository
    )
    {
        $this->repository = $repository;
        $this->siteTypesRepository = $siteTypesRepository;

        $this->middleware('can:view,"mdcms_sites"')->only('index');
        $this->middleware('can:add,"mdcms_sites"')->only('create', 'store');
        $this->middleware('can:edit,"mdcms_sites"')->only('edit', 'update');
        $this->middleware('can:delete,"mdcms_sites"')->only('destroy');

        $this->panelMenu()->setActive('sites');
    }

    public function index(string $lang)
    {
        $sites = $this->repository->getAll(10, $lang);

        return view('mdcms-sites::panel.index')->with([
            'sites' => $sites,
            'siteTypes' => $this->siteTypesRepository->getAllSiteTypes()
        ]);
    }

    public function create(string $lang)
    {
        return view('mdcms-sites::panel.create')->with([
            'typesSelectData' => $this->siteTypesRepository->getDataForSelect($lang),
            'allSiteTypes' => $this->siteTypesRepository->getAllSiteTypes(),
            'pluginsConfig' => $this->siteTypesRepository->getPluginsConfig(),
        ]);
    }

    public function store(StoreOrEditSite $request, string $lang)
    {
        $data = $request->validated();
        $data['lang'] = $lang;

        $site = $this->repository->add($data);
        flash()->ok(__p('Site added!'));

        $this->reloadRouteCacheIfNecessary($data);

        return $this->redirectAfterSaveButton('mdcms.panel.sites.index', ['id' => $site->id]);
    }

    protected function reloadRouteCacheIfNecessary(array $data, object $site = null)
    {
        $type = $data['type'] ?? null;
        $url = $data['url'] ?? null;
        $active = $data['active'] ?? null;
        $oldType = $site->type ?? null;
        $oldUrl = $site->url ?? null;
        $oldActive = $site->active ?? null;
        $siteTypes = $this->siteTypesRepository->getOnlySitesModuleTypes();

        if (!$oldType) {
            $reload = !array_key_exists($type, $siteTypes);
        } else {
            $oneOfTypeIsNotSite = (!array_key_exists($type, $siteTypes) || !array_key_exists($oldType, $siteTypes));

            $typeChanged = $type !== $oldType && $oneOfTypeIsNotSite;
            $urlChanged = $url !== $oldUrl && $oneOfTypeIsNotSite;
            $activeChanged = $active != $oldActive && $oneOfTypeIsNotSite;

            $reload = $typeChanged || $urlChanged || $activeChanged;
        }

        if ($reload) {
            RouteCacheReload::dispatch();
        }
    }

    public function edit(string $lang, int $id)
    {
        $site = $this->repository->get($id, $lang);

        if(!$site) {
            flash()->warning(__p('Item not found!'));
            return redirect()->route('mdcms.panel.sites.index');
        }

        return view('mdcms-sites::panel.edit')->with([
            'typesSelectData' => $this->siteTypesRepository->getDataForSelect($lang, $site->id),
            'allSiteTypes' => $this->siteTypesRepository->getAllSiteTypes(),
            'pluginsConfig' => $this->siteTypesRepository->getPluginsConfig(),
            'site' => $site,
        ]);
    }

    public function update(StoreOrEditSite $request, string $lang, int $id)
    {
        $data = $request->validated();
        $site = $this->repository->get($id, $lang);

        $this->repository->edit($id, $data);
        flash()->ok(__p('Site Updated!'));

        $this->reloadRouteCacheIfNecessary($data, $site);

        return $this->redirectAfterSaveButton('mdcms.panel.sites.index', ['id' => $id]);
    }

    public function destroy(string $lang, int $id)
    {
        $site = $this->repository->get($id, $lang);

        if(!$site) {
            flash()->warning(__p('Item not found!'));
            return redirect()->route('mdcms.panel.sites.index');
        }

        $siteData = $site->toArray();

        $this->repository->delete($id);
        flash()->ok(__p('Site deleted!'));

        $this->reloadRouteCacheIfNecessary($siteData);

        return redirect()->route('mdcms.panel.sites.index');
    }
}
