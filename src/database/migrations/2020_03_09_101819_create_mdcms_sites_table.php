<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdcmsSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mdcms_sites', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('url');
            $table->string('name');
            $table->json('fields')->nullable();
            $table->string('type')->default('site');
            $table->unsignedTinyInteger('active')->default(1);
            $table->char('lang', 2);
            $table->timestamps();

            $table->unique(['url', 'lang']);

            $table->foreign('lang')
                ->references('code')->on('mdcms_languages')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mdcms_sites');
    }
}
