<?php

namespace MdProject\MdCmsSites;

use Illuminate\Support\ServiceProvider;
use MdProject\MdCmsNavigations\Resolver\NavigationResolver;
use MdProject\MdCmsSites\SitesNavigationProvider;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Gate;

class MdCmsSitesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/config/mdcms_sites.php',
            'mdcms_sites'
        );

        $this->app->bind(
            'MdProject\MdCmsSites\Repositories\SiteRepository',
            'MdProject\MdCmsSites\Repositories\SiteRepositoryEloquent'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(NavigationResolver $resolver, SitesNavigationProvider $sitesProvider)
    {
        /* router */
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');

        /* config */
        $this->publishes([
            __DIR__ . '/config/mdcms_sites.php' => config_path('mdcms_sites.php'),
        ], 'config');

        /* migrations */
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');

        /* views */
        $this->publishes([
            __DIR__ . '/views/front' => resource_path('views/sites'),
        ], 'views');

        if (isPanel()) {
            $this->loadViewsFrom(__DIR__.'/views', 'mdcms-sites');
        } else {
            $this->loadViewsFrom(__DIR__.'/views/front', 'mdcms-sites');
        }

        $resolver->addProvider($sitesProvider);

        Gate::policy('mdcms_sites', \MdProject\MdCmsSites\Policies\SitesPolicy::class);

        Event::listen('MdProject\MdCmsPanel\Events\ItemDeleted', 'MdProject\MdCmsSites\Listeners\ItemDeleted');
    }
}
