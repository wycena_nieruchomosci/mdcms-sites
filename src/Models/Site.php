<?php

namespace MdProject\MdCmsSites\Models;

use Illuminate\Database\Eloquent\Model;
use MdProject\MdCmsItemPlugins\Traits\ItemPlugins;

class Site extends Model
{
    use ItemPlugins;

    protected $table = 'mdcms_sites';

    protected $fillable = [
        'url', 'name', 'fields', 'type', 'active', 'lang'
    ];

    protected $casts = [
        'fields' => 'array',
    ];

    public function getPluginsConfig(): array
    {
        $typesConfig = resolve('MdProject\MdCmsSites\Repositories\SiteTypesRepository')->getAllSiteTypes();
        return $typesConfig[$this->type]['plugins'] ?? [];
    }
}
