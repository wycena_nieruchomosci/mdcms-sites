<?php

namespace MdProject\MdCmsSites\Models;

use MdProject\MdCmsSites\Models\Site as BaseSite;
use MdProject\MdCmsBlocks\Traits\Blocks;

class SiteWithBlocks extends BaseSite
{
    use Blocks;
}
