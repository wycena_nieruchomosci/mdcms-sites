@extends('mdcms-panel::layouts.admin')

@section('content')
@title
<i class="fa icon-docs"></i>
{{ __p('Add new site') }}
@endtitle

@if(View::exists('mdcms-item-plugins::buttons'))
    @include('mdcms-sites::panel.partials.item-plugins')
@endif

<form class="container-fluid" action="{{ route('mdcms.panel.sites.store') }}" method="POST">
    @csrf
    @include('mdcms-sites::panel.partials.form-content')
</form>
@endsection
