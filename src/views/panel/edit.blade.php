@extends('mdcms-panel::layouts.admin')

@section('content')
@title
<i class="fa icon-docs"></i>
{{ $site->name }}
@endtitle

@if(View::exists('mdcms-item-plugins::buttons'))
    @include('mdcms-sites::panel.partials.item-plugins')
@endif

<form class="container-fluid" action="{{ route('mdcms.panel.sites.update', $site->id) }}" method="POST">
    @method('PUT')
    @csrf
    @include('mdcms-sites::panel.partials.form-content')
</form>
@endsection
