@extends('mdcms-panel::layouts.admin')

@section('content')
@title
<i class="fa icon-docs"></i>
{{ __p('Sites list') }}
@endtitle


<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <i class="fa icon-docs"></i>
                        {{ __p('Sites') }}
                        <div class="card-header-actions">
                            @can('add', 'mdcms_sites')
                            <a class="btn btn-success btn-sm" href="{{ route('mdcms.panel.sites.create') }}">
                                <i class="fa fa-plus"></i>
                            </a>
                            @endcan
                        </div>
                    </div>
                    <div class="card-body">
                        <ul class="list-group">
                            @foreach ($sites as $site)
                            <li class="list-group-item d-flex list-group-item-action justify-content-between align-items-center">
                                <div>
                                    @if($site->active == 1)
                                    <i class="fa fa-circle text-success"></i>
                                    @else
                                    <i class="fa fa-circle text-secondary"></i>
                                    @endif
                                    {{ $site->name }}
                                    <div>
                                        <span class="badge badge-info text-white">
                                            {{ __p($siteTypes[$site->type]['name'] ?? $site->type) }}
                                        </span>
                                    </div>
                                </div>
                                <div>
                                    @include('mdcms-panel::partials.lists-elements.std-buttons', [
                                        'item' => $site,
                                        'editRoute' => 'mdcms.panel.sites.edit',
                                        'policyContext' => 'mdcms_sites',
                                    ])
                                </div>
                            </li>
                            @endforeach
                        </ul>

                        @can('delete', 'mdcms_sites')
                            @include('mdcms-panel::partials.lists-elements.delete-confirm-modal', [
                                'items' => $sites,
                                'route' => 'mdcms.panel.sites.destroy'
                            ])
                        @endcan

                        {{ $sites->links('mdcms-panel::partials.pagination') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

@endpush
