<div
    id="sites-blocks-wrapper"
    class="d-none"
    @foreach($allSiteTypes as $type => $config)
        @if($config['blocks'] ?? false)
            {{ "data-$type=1" }}
        @endif
    @endforeach
>
    @if(View::exists('mdcms-blocks::blocks'))
        @include('mdcms-blocks::blocks',[
            'title' => 'Content blocks',
            'item' => $site ?? null,
        ])
    @endif
</div>

@push('scripts')
    <script>
        (function(){
            const wrapper = $('#sites-blocks-wrapper');
            const typeSelector = $('select[name=type]');

            function switchBlocksView() {
                wrapper.addClass('d-none');

                const type = typeSelector.val();
                if (wrapper.data(type) === 1) {
                    wrapper.removeClass('d-none');
                }
            }

            switchBlocksView();
            typeSelector.change(switchBlocksView);
        })();
    </script>
@endpush
