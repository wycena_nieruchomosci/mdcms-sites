<div
    id="site-plugins"
    data-config='@json($pluginsConfig["byType"])'
    class="d-none"
>
    @include('mdcms-item-plugins::buttons', [
        'item' => $site ?? null,
        'config' => $pluginsConfig['all'],
    ])
</div>
@push('scripts')
    <script>
        (function(){
            const wrapper = $('#site-plugins');
            const config = wrapper.data('config');
            const typeSelector = $('select[name=type]');

            function switchPluginsView() {
                wrapper.addClass('d-none');

                const type = typeSelector.val();
                const conf = config[type];

                let plugins = wrapper.find('[data-type]');
                for (let plugin of plugins) {
                    let pluginSelector = $(plugin);
                    if (conf.includes(pluginSelector.data('type'))) {
                        pluginSelector.removeClass('d-none');
                    } else {
                        pluginSelector.addClass('d-none');
                    }
                }

                wrapper.removeClass('d-none');
            }

            switchPluginsView();
            typeSelector.change(switchPluginsView);
        })();
    </script>
@endpush
