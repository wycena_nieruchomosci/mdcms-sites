<div class="animated fadeIn">
    <div class="row">
        <div class="col-12 order-2 col-xl-6 order-xl-1">
            <div class="card">
                <div class="card-header">
                    <i class="fa icon-note"></i>
                    {{ __p('Content fields') }}
                </div>
                @include('mdcms-panel::fields.mass-bulider-fields-sets', [
                    'fieldsSets' => $allSiteTypes,
                    'changerSelector' => '#type',
                    'currentValues' => old('fields', $site->fields ?? null)
                ])
            </div>
            @if(View::exists('mdcms-blocks::blocks'))
                @include('mdcms-sites::panel.partials.blocks')
            @endif
        </div>
        <div class="col-12 order-1 col-xl-6 order-xl-2">
            <div class="card">
                <div class="card-header">
                    <i class="fa icon-settings"></i>
                    {{ __p('Settings') }}
                </div>
                <div class="card-body">
                    @include('mdcms-panel::fields.input', [
                        'label' => __p('Name'),
                        'name' => 'name',
                        'value' => old('name', $site->name ?? null),
                        'id' => 'name',
                        'type' => 'text',
                        'placeholder' => __p('Site name'),
                    ])
                    @include('mdcms-panel::fields.input', [
                        'label' => __p('Url'),
                        'name' => 'url',
                        'value' => old('url', $site->url ?? null),
                        'id' => 'url',
                        'type' => 'text',
                        'placeholder' => __p('Site url in link'),
                        'tooltip' => __p('Url is used in the address bar and must be unique. Will be created automatically from name when field is empty.'),
                    ])
                    @include('mdcms-panel::fields.select', [
                        'label' => __p('Type'),
                        'name' => 'type',
                        'id' => 'type',
                        'options' => $typesSelectData['options'],
                        'selected' => old('type', $site->type ?? null),
                        'disabledOptions' => $typesSelectData['disabledOptions']
                    ])
                    @include('mdcms-panel::fields.switch', [
                        'label' => __p('Active'),
                        'name' => 'active',
                        'value' => 1,

                        'checked' => old('active', $site->active ?? 1),
                        'color' => 'success'
                    ])
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="bg-light p-3 border d-flex justify-content-between">
                <a class="btn btn-dark" type="reset" href="{{ route('mdcms.panel.sites.index') }}">
                    <i class="fa fa-arrow-left"></i> {{ __p('Back') }}</a>
                <div>
                    <button class="btn btn-info text-white" type="submit" name="back_to_edit" value="1">
                        <i class="fa fa-check"></i> {{ __p('Save and edit') }}</button>
                    <button class="btn btn-success" type="submit">
                        <i class="fa fa-check"></i> {{ __p('Save') }}</button>
                </div>
            </div>
        </div>
    </div>
</div>
