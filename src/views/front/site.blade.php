{{--
    $site
--}}

{{ $site->name }}

@if(View::exists('mdcms-front::gallery-plugin'))
    @include('mdcms-front::gallery-plugin', ['item' => $site])
@endif

@if(View::exists('mdcms-blocks::blocks'))
    @include('mdcms-blocks::blocks')
@endif
