<?php

namespace MdProject\MdCmsSites;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Config;

class FrontRoute
{
    static function load()
    {
        if (Config::get('mdcms_languages.multilanguage')) {
            Route::get('/{lang?}', '\MdProject\MdCmsSites\Controllers\FrontSitesController@home')->name('home');
            Route::get('/{lang}/{url}', '\MdProject\MdCmsSites\Controllers\FrontSitesController@site')->name('site');
        } else {
            Route::get('/', '\MdProject\MdCmsSites\Controllers\FrontSitesController@home')->name('home');
            Route::get('/{url}', '\MdProject\MdCmsSites\Controllers\FrontSitesController@site')->name('site');
        }
    }

    static function getHomeUrl(string $lang = null)
    {
        if (Config::get('mdcms_languages.multilanguage')) {
            if (Config::get('mdcms_languages.front_locale') == $lang) {
                return '/';
            } else {
                return "/$lang";
            }
        } else {
            return '/';
        }
    }
}
