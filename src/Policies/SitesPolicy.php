<?php

namespace MdProject\MdCmsSites\Policies;

use MdProject\MdCmsAuth\Policies\BaseUserPermissionsPolicy;

class SitesPolicy extends BaseUserPermissionsPolicy
{
    public static $defaultActions = ['view', 'add', 'edit', 'delete'];
    public static $name = 'Sites module';
}
