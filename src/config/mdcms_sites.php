<?php

return [
    'site_types' => [
        'home' => [
            'name' => 'Home site',
            'unique' => true,
            'order' => 1,
            'fields' => [
                [
                    'type' => 'editor',
                    'settings' => [
                        'name' => 'content',
                        'label' => 'Site content',
                    ]
                ],
            ],
            'plugins' => ['seo'],
        ],
        'site' => [
            'name' => 'Site',
            'order' => 0,
            'fields' => [
                [
                    'type' => 'editor',
                    'settings' => [
                        'name' => 'content',
                        'label' => 'Site content'
                    ]
                ]
            ],
            'plugins' => ['seo'],
            'blocks' => true,
        ],
        'example' => [
            'name' => 'Fields example',
            'order' => 2,
            'fields' => [
                [
                    'type' => 'editor',
                    'settings' => [
                        'name' => 'content1',
                        'label' => 'Editor mini',
                        'type' => 'mini'
                    ]
                ],
                [
                    'type' => 'editor',
                    'settings' => [
                        'name' => 'content2',
                        'label' => 'Editor basic',
                        'type' => 'basic'
                    ]
                ],
                [
                    'type' => 'editor',
                    'settings' => [
                        'name' => 'content3',
                        'label' => 'Editor'
                    ]
                ],
                [
                    'type' => 'textarea',
                    'settings' => [
                        'name' => 'field1',
                        'label' => 'Textarea'
                    ]
                ],
                [
                    'type' => 'input',
                    'settings' => [
                        'name' => 'field2',
                        'label' => 'Input',
                    ]
                ],
                [
                    'type' => 'input',
                    'settings' => [
                        'name' => 'field3',
                        'label' => 'Input - date',
                        'type' => 'date'
                    ]
                ],
            ],
            'plugins' => ['seo', 'gallery'],
        ],
    ],
    'panel_menu' => [
        [
            'key' => 'sites',
            'action' => '\MdProject\MdCmsSites\Controllers\SitesController@index',
            'label' => 'Sites',
            'data' => ['icon' => 'icon-docs', 'policyContext' => 'mdcms_sites'],
            'order' => 2,
        ]
    ]
];
