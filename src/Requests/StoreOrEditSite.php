<?php

namespace MdProject\MdCmsSites\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class StoreOrEditSite extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'type' => [
                'required',
                'max:255',
                function ($attribute, $value, $fail) {
                    $typesRepo = resolve('MdProject\MdCmsSites\Repositories\SiteTypesRepository');
                    $types = $typesRepo->getAllSiteTypes();
                    if (!array_key_exists($value, $types)) {
                        $fail(__p('Site type is invalid.'));
                    }
                },
            ],
            'fields' => 'array|nullable',
            'url' => [
                'required',
                'max:255',
                'unique_per_language:table=mdcms_sites,skipId='.$this->id,
                'regex:/^[a-zA-Z0-9_\-]+$/'
            ],
            'active' => 'nullable|in:1',
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'url' => Str::slug($this->url ?? $this->name),
        ]);
    }
}
