<?php

namespace MdProject\MdCmsSites;

use MdProject\MdCmsNavigations\Resolver\NavigationProviderInterface;
use MdProject\MdCmsSites\Repositories\SiteRepository;
use Illuminate\Http\Request;

class SitesNavigationProvider implements NavigationProviderInterface
{
    protected $idNameUrl;
    protected $siteRepository, $request;

    public function __construct(SiteRepository $siteRepo, Request $request)
    {
        $this->siteRepository = $siteRepo;
        $this->request = $request;
    }

    public function getLinkType(): string
    {
        return 'mdcms_sites';
    }

    public function getName(): string
    {
        return __p('Site');
    }

    public function getFieldsSettings(): array
    {
        $this->loadDataIfNotLoaded();

        return [
            [
                'type' => 'select',
                'settings' => [
                    'label' => __p('Site'),
                    'name' => 'id',
                    'options' => $this->idNameUrl->pluck('name', 'id'),
                ],
            ],
        ];
    }

    protected function loadDataIfNotLoaded()
    {
        if (empty($this->idNameUrl)) {
            $lang = $this->request->route('lang');
            $this->idNameUrl = $this->siteRepository->getDataForNavigation($lang);
        }
    }

    public function getItem(array $fields): array
    {
        $this->loadDataIfNotLoaded();
        $item = $this->idNameUrl->where('id', $fields['id'])->first();

        return [
            'key' => $item ? 'site_'.$item->id : null,
            'name' => $item->name ?? '',
            'link' => $item ? $this->getUrl($item->type, $item->url) : '',
        ];
    }

    private function getUrl(string $type, string $url)
    {
        if($type == 'home') {
            $lang = $this->request->route('lang');
            $link = \MdProject\MdCmsSites\FrontRoute::getHomeUrl($lang);
        } else {
            $link = route('site', ['url' => $url]);
        }

        return $link;
    }
}
